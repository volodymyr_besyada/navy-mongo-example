import React from 'react';
import PropTypes from 'prop-types';
import './CounterControl.css';

const counterControl = ({ clicked, label, index }) => (
  <div className="CounterControl" onClick={clicked} role="button" tabIndex={index}>
    {label}
  </div>
);

export default counterControl;


counterControl.propTypes = {
  clicked: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
  index: PropTypes.string.isRequired,
};
