import React from 'react';
import PropTypes from 'prop-types';
import './CounterOutput.css';

/**
 * Counter output will be used for display counter value.
 * @param props.value current value from store.
 */
const counterOutput = ({ value }) => (
  <div className="CounterOutput">
        Redux counter:
    {value}
  </div>
);

export default counterOutput;

counterOutput.propTypes = {
  value: PropTypes.number.isRequired,
};
