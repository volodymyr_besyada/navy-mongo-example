import React from 'react';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import './MainMenu.css';


const MainMenu = () => {
  const useStyles = makeStyles((theme) => ({
    button: {
      margin: theme.spacing(1),
    },
    input: {
      display: 'block',
    },
  }));

  const classes = useStyles();

  return (
    <div className="MainMenu">
      <div>
        <Button variant="outlined" color="primary" className={classes.button}>
          Sign Up
        </Button>
        <Button variant="outlined" color="primary" className={classes.button}>
          Log In
        </Button>
      </div>
    </div>

  );
};


export default MainMenu;
