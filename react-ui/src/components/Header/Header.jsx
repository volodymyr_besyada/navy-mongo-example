import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import {
  AppBar,
  Toolbar,
  Drawer,
  Hidden,
  IconButton,
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  menuLogo: {
    color: '#fff',
    textDecoration: 'none',
    margin: '0 20px',
    fontSize: '30px',
    fontWeight: 'bold',
  },
  menuLink: {
    color: '#fff',
    textDecoration: 'none',
    margin: '0 20px',
    borderBottom: '2px solid transparent',
    transition: '.2s ease-in-out',
    '&:hover': {
      borderBottom: '2px solid #fff',
    },
  },
  menuLinkDrawer: {
    textDecoration: 'none',
    padding: '10px 40px',
    margin: '10px',
    fontSize: '24px',
  },
  menuLogin: {
    color: '#fff',
    marginLeft: 'auto',
  },
  menuBtn: {
    display: 'none',
    [theme.breakpoints.down('xs')]: {
      display: 'block',
    },
  },
}));

function Header() {
  const classes = useStyles();
  const [showDrawer, setShowDrawer] = useState(false);
  function toggleDrawer() {
    setShowDrawer(!showDrawer);
  }
  return (
    <>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            className={classes.menuBtn}
            onClick={toggleDrawer}
            color="inherit"
          >
            <MenuIcon />
          </IconButton>
          <Hidden xsDown>
            <Link to="/" className={classes.menuLogo}>LOGO</Link>
            <Link to="/" className={classes.menuLink}>Home</Link>
            <Link to="/about/" className={classes.menuLink}>About</Link>
            <Link to="/users/" className={classes.menuLink}>Users</Link>
          </Hidden>
          <Link to="/" className={classes.menuLogin}>
            <AccountCircle />
          </Link>
        </Toolbar>
      </AppBar>
      <Hidden smUp>
        <Drawer open={showDrawer} onClose={toggleDrawer}>
          <Link to="/" className={classes.menuLinkDrawer} onClick={toggleDrawer}>Home</Link>
          <Link to="/about/" className={classes.menuLinkDrawer} onClick={toggleDrawer}>About</Link>
          <Link to="/users/" className={classes.menuLinkDrawer} onClick={toggleDrawer}>Users</Link>
        </Drawer>
      </Hidden>
    </>
  );
}

export default Header;
