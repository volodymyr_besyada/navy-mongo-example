import React from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import MainMenu from './MainMenu/MainMenu';
import Logo from './Logo/Logo';
import './Header.css';

const Header = () => (
  <div className="Header">

    <Grid
      container
      spacing={2}
      justify="space-between"
      alignItems="center"
      alignContent="flex-end"
    >

      <Grid item xs={2}>
        <Logo />
      </Grid>
      <Grid item xs={7}>
        <TextField
          id="standard-search"
          label="Try to find a product"
          type="search"
          className="Search"
          margin="normal"
        />
      </Grid>
      <Grid item xs={3}>
        <MainMenu />
      </Grid>
    </Grid>
  </div>

);

export default Header;
