import React from 'react';
import Button from '@material-ui/core/Button';
import LogoIcon from '@material-ui/icons/OpenWith';

import './Logo.css';
import { makeStyles } from '@material-ui/core';


const Logo = () => {
  const useStyles = makeStyles((theme) => ({
    button: {
      margin: theme.spacing(1),
    },
    input: {
      display: 'block',
    },
  }));

  const classes = useStyles();

  return (
    <div className="Logo">
      <div>
        <Button href="/" alt="Home" ariant="outlined" color="primary" className={classes.button}>
          <LogoIcon />
          {' '}
NavY LaB
        </Button>
      </div>
    </div>

  );
};


export default Logo;
