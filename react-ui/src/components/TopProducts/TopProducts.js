import React from 'react';
import Grid from '@material-ui/core/Grid';
import ProductItem from './ProductItem/ProductItem';
import './TopProducts.css';

const TopProducts = () => (
  <div className="TopProducts">

    <Grid container spacing={2}>

      <Grid item xs={1} md={2} lg={4} className="ProductItemBlock">
        <ProductItem />
      </Grid>
      <Grid item xs={1} md={2} lg={4} className="ProductItemBlock">
        <ProductItem />
      </Grid>
      <Grid item xs={1} md={2} lg={4} className="ProductItemBlock">
        <ProductItem />
      </Grid>
      <Grid item xs={1} md={2} lg={4} className="ProductItemBlock">
        <ProductItem />
      </Grid>
      <Grid item xs={1} md={2} lg={4} className="ProductItemBlock">
        <ProductItem />
      </Grid>
      <Grid item xs={1} md={2} lg={4} className="ProductItemBlock">
        <ProductItem />
      </Grid>
      <Grid item xs={1} md={2} lg={4} className="ProductItemBlock">
        <ProductItem />
      </Grid>
      <Grid item xs={1} md={2} lg={4} className="ProductItemBlock">
        <ProductItem />
      </Grid>
      <Grid item xs={1} md={2} lg={4} className="ProductItemBlock">
        <ProductItem />
      </Grid>
    </Grid>
  </div>

);


export default TopProducts;
