import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import App from '../pages/App/App';
import Counter from '../containers/Counter/Counter';


import Header from '../components/Header/Header';

function About() {
  return <h2>The About Section</h2>;
}

function Users() {
  return <h2>The Users Section</h2>;
}

function AppRouter() {
  return (
    <Router>
      <div>
        <Header />
        <Route path="/" exact component={App} />
        <Route path="/about/" component={About} />
        <Route path="/users/" component={Users} />
        <Route path="/counter/:id" component={Counter} />
      </div>
    </Router>
  );
}

export default AppRouter;
