import React, { useCallback, useEffect, useState } from 'react';
import './App.css';

// Import counter container.
import Container from '@material-ui/core/Container';
import Counter from '../../containers/Counter/Counter';
import Header from '../../components/Header/Header';
import TopProducts from '../../components/TopProducts/TopProducts';

function App() {
  const [message, setMessage] = useState(null);
  const [isFetching, setIsFetching] = useState(false);
  const [url/* , setUrl */] = useState('/api');


  const fetchData = useCallback(() => {
    fetch(url)
      .then((response) => {
        if (!response.ok) {
          throw new Error(`status ${response.status}`);
        }
        return response.json();
      })
      .then((json) => {
        setMessage(json.message);
        setIsFetching(false);
      }).catch((e) => {
        setMessage(`API call failed: ${e}`);
        setIsFetching(false);
      });
  }, [url]);

  useEffect(() => {
    setIsFetching(true);
    fetchData();
  }, [fetchData]);


  return (
    <div className="App">
      <Container>
        <Header />
        <div>
          <br />
          <br />
          <br />
          <br />
          <h3>
            Should be a form with product filters...
          </h3>
          <br />
          <br />
          <br />
          <br />
        </div>

        <div>
          <TopProducts />
        </div>

        <div className="App-heasder">
          {process.env.NODE_ENV === 'production'
            ? (
              <p>
                This is a production build from create-react-app.
              </p>
            )
            : (
              <p>
                Edit
                {' '}
                <code>src/App.js</code>
                {' '}
                and save to reload.
              </p>
            )}
          <p>
            {'« '}
            <strong>
              {isFetching
                ? 'Fetching message from API'
                : message}
            </strong>
            {' »'}
          </p>
          <p>
            <a
              className="App-link"
              href="https://github.com/mars/heroku-cra-node"
            >
              React + Node deployment on Heroku
            </a>
          </p>
          <p>
            <a
              className="App-link"
              href="https://reactjs.org"
              target="_blank"
              rel="noopener noreferrer"
            >
              Learn React
            </a>
          </p>
        </div>
        <hr />
        <p>counter section build with redux</p>
        <Counter />


      </Container>

    </div>
  );
}

export default App;
