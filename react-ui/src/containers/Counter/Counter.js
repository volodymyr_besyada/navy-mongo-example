import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import PropTypes from 'prop-types';
import CounterControl from '../../components/CounterControl/CounterControl';
import CounterOutput from '../../components/CounterOutput/CounterOutput';

// import actions from the corresponding folder rather than creating them here
import { onIncrementCounter, onDecrementCounter, fetchUsetrs, fetchUsetr } from '../../actions/counterActions';

class Counter extends PureComponent {

    componentDidMount() {
        const { getUser, fet, match } = this.props;
        const {id} = match.params;
        // fet();
        getUser(id);
    }

    render() {
    const { ctr, incrementCounter, decrementCounter, fet } = this.props;
console.log(this.props);
    return (
      <div>
        <CounterOutput value={ctr} />
        <CounterControl label="Increment" index="1" clicked={incrementCounter} />
        <CounterControl label="Decrement" index="2" clicked={decrementCounter} />
      </div>
    );
  }
}

// state.conter - used to be property "counter" in general state,
// but now there is a state object that comes
// from counterReducer and in that object we can find property "counter"
const mapStateToProps = (state) => ({
  ctr: state.counter.counter,
  users: state.counter.users,
  user: state.counter.user,
});

// these actions can be more complex in the future and now they are in actions folder.
/* const mapDispatchToProps = (dispatch) => ({
  onIncrementCounter: () => dispatch({ type: 'INCREMENT' }),
  onDecrementCounter: () => dispatch({ type: 'DECREMENT' }),
}); */

// we import these actions from the folder, but to use them as props we must include them here
export default connect(mapStateToProps, {
  incrementCounter: onIncrementCounter,
  decrementCounter: onDecrementCounter,
  fet: fetchUsetrs,
  getUser: fetchUsetr,
})(Counter);

Counter.propTypes = {
  incrementCounter: PropTypes.func.isRequired,
  decrementCounter: PropTypes.func.isRequired,
  ctr: PropTypes.number.isRequired,
};
