import {INCREMENT, DECREMENT, INCREMENT_ERR, ALLUSERS, GETUSER} from '../actions/types';

// Define default state.
const initialState = {
  counter: 5,
  users:[],
  user:{}
};


const counterReducer = (state = initialState, action) => {
  switch (action.type) {
    case INCREMENT:
      return { counter: state.counter + 1 };
    case DECREMENT:
      return { counter: state.counter - 1 };
    case ALLUSERS:
      return { users: action.users };
    case GETUSER:
      return { ...state, user: action.user };
    case INCREMENT_ERR:
      // eslint-disable-next-line
      console.error(action.payload);
      return {
        ...state,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default counterReducer;
