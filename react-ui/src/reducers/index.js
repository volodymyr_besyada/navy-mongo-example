import { combineReducers } from 'redux';
import counterReducer from './counterReducer';
// all the reducers will be combined here and imported in store file as one
export default combineReducers({
  counter: counterReducer,
});
