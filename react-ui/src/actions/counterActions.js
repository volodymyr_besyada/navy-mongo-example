import { INCREMENT, INCREMENT_ERR, DECREMENT, ALLUSERS, GETUSER } from './types';
import axois from 'axios';

export const onIncrementCounter = () => async (dispatch) => {
  try {
    dispatch({
      type: INCREMENT,
    });
  } catch (err) {
    dispatch({
      type: INCREMENT_ERR,
      payload: err,
    });
  }
};

export const onDecrementCounter = () => async (dispatch) => {
  try {
    dispatch({
      type: DECREMENT,
    });
  } catch (err) {
    dispatch({
      type: INCREMENT_ERR,
      payload: err,
    });
  }
};
export const fetchUsetrs = () => async (dispatch) => {
  axois.get('/api/getallusers').then((res) => {
    dispatch({
      type: ALLUSERS,
      users: res.data,
    });
  });
};
export const fetchUsetr = (id) => async (dispatch) => {
  axois.get(`/api/getuserbyid/${id}`).then((res) => {
    dispatch({
      type: GETUSER,
      user: res.data,
    });
  });
};
