// it's better to use variables containing action types.
// Smaller chance to make a mistake when writing a string each time.
// variables pop up when we write, therefore less time on errors

export const INCREMENT = 'INCREMENT';
export const INCREMENT_ERR = 'INCREMENT_ERR';
export const DECREMENT = 'DECREMENT';
export const ALLUSERS = 'ALLUSERS';
export const GETUSER = 'GETUSER';
