var mongoose = require('mongoose');

var usersSchema = new mongoose.Schema({
    name: String,
    date: { type: Date, default: Date.now },
});

var Users = mongoose.model('Users', usersSchema);

module.exports = Users;
